import gulp from 'gulp';
import watch from 'gulp-watch';
import browserSync from 'browser-sync';

const { reload } = browserSync;

gulp.task('reload', (done) => {
  reload();
  done();
});

gulp.task('watcher', (done) => {
  global.isWatching = true;
  watch([
    './src/styles/*.styl',
    './src/styles/includes/*',
    './src/styles/svg/*.svg',
    './src/blocks/**/*.styl',
    './src/blocks/**/**/*.styl',
  ], gulp.series('stylint', 'stylus', 'reload'));
  watch([
    './src/blocks/**/*.pug',
    './src/blocks/**/**/*.pug',
    './src/pages/**/*.pug',
    './src/data/*',
  ], gulp.series('template', 'reload'));
  watch([
    './src/blocks/**/*.js',
    './src/scripts/script.js',
    './src/scripts/modules/*.js',
  ], gulp.series('script', 'reload'));
  watch('./src/resources/**', gulp.series('copy', 'reload'));
  done();
});
