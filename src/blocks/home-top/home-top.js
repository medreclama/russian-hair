export default function homeTopPar() {
  const parallax = () => {
    const ypos = window.pageYOffset;
    const par = document.getElementById('parallax');
    if (par) {
      par.style.transform = `translateY(${ypos * 0.5}px)`;
    }
  };
  window.addEventListener('scroll', parallax);
}
