export default function pricesSwitch() {
  function currencySwitch() {
    const priceTables = this.parentElement.parentElement.parentElement.querySelectorAll('.price-block__currency-table');
    const priceSwitchersCurrentBlock = this.parentElement.querySelectorAll('.price-block__currency-title');
    for (let j = 0; j < priceTables.length; j += 1) {
      priceTables[j].style.display = 'none';
      priceSwitchersCurrentBlock[j].classList.remove('price-block__currency-title_current');
      if (this.dataset.currency === priceTables[j].dataset.currency) {
        priceTables[j].style.display = 'table';
      }
    }
    this.classList.add('price-block__currency-title_current');
  }

  const priceSwitchers = document.querySelectorAll('.price-block__currency-title');
  const priceSwitchersFirst = document.querySelectorAll('.price-block__currency-title:first-of-type');
  for (let i = 0; i < priceSwitchersFirst.length; i += 1) {
    priceSwitchersFirst[i].classList.add('price-block__currency-title_current');
  }

  for (let i = 0; i < priceSwitchers.length; i += 1) {
    priceSwitchers[i].addEventListener('click', currencySwitch);
  }
}
