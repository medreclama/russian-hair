
function sublistToggle() {
  const sublist = this.parentElement.parentElement.lastElementChild;
  if (sublist.classList.contains('header-mobile-menu__sublist')) {
    this.classList.toggle('header-mobile-menu__sublist-switcher--active');
    sublist.classList.toggle('header-mobile-menu__sublist--active');
  }
}

export default function headerMobileMenu() {
  const mobileMenuItems = Array.from(document.querySelectorAll('.header-mobile-menu__item'));
  mobileMenuItems.forEach((item) => {
    if (item.lastElementChild.className === 'header-mobile-menu__sublist') {
      const parentSwitcher = document.createElement('div');
      parentSwitcher.classList.add('header-mobile-menu__sublist-switcher');
      parentSwitcher.addEventListener('click', sublistToggle);
      const itemBody = item.querySelector('.header-mobile-menu__item-body');
      itemBody.appendChild(parentSwitcher);
    }
  });
}
