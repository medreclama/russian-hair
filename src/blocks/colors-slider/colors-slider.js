import $ from 'jquery';

export default function colorsSlider() {
  $('.colors-slider').flexslider({
    animation: 'slide',
    animationLoop: false,
    itemWidth: 100,
    itemMargin: 21,
    autoplay: false,
  });
}
