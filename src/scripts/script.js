import $ from 'jquery'; // eslint-disable-line no-unused-vars
import fancybox from '@fancyapps/fancybox'; // eslint-disable-line no-unused-vars
import flexslider from 'flexslider'; // eslint-disable-line no-unused-vars
import tabs from '../blocks/tabs/tabs';
import headerMobileMenu from '../blocks/header-mobile/header-mobile-menu/header-mobile-menu';
import homeTopPar from '../blocks/home-top/home-top';
import pricesSwitch from '../blocks/price-block/price-block';
import colorsSlider from '../blocks/colors-slider/colors-slider';

headerMobileMenu();
homeTopPar();
pricesSwitch();

new Promise((res) => {
  colorsSlider();
  setTimeout(() => {
    res(true);
  }, 100);
}).then(() => {
  tabs();
});
